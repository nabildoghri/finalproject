import {createStore} from "redux";
import bookReducer from "./reducers";

export const bookStore = createStore(bookReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
bookStore.subscribe(()=>console.log("### Book Store State ###"+JSON.stringify(bookStore.getState())))// voir l'etat du store
export default bookStore;