
import './App.css';
import {Provider } from 'react-redux'
import bookStore from './store'
import { BrowserRouter as Router} from "react-router-dom";

import { SideBar } from './components/SideBar';
import { Content } from './components/Content';
import { Footer } from './components/Footer';
function App() {
  return (
    <Provider store={bookStore}>
      <div className="App wrapper" >
      <Router>
        <SideBar/>
        <Content/>
        <Footer/>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
