import {  Route, Link } from "react-router-dom";
import { useState } from 'react';
export function Menue(){
    const NAV_LINK = "nav-link"
    const NAV_LINK_ACTIVE = "nav-link active"
    const [homeLink, setHomeLinkStatus] = useState(true)
    const [addLink, setAddLinkStatus] = useState(false)
    const [aboutLink, setAboutLinkStatus] = useState(false)
    return <div>
       <ul>
            <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li className="nav-item menu-open">
                    <Route>
                        <Link to="/" className={homeLink?NAV_LINK_ACTIVE: NAV_LINK} onClick={()=>{setHomeLinkStatus(true)
                                setAddLinkStatus(false)
                                setAboutLinkStatus(false)
                                }}><i className="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                            <i className="right fas fa-angle-left"></i>
                            Home
                            </p>
                        </Link>
                    </Route>

                    <li className="nav-item">
                        <Route>
                            <Link to="/addBook" className={addLink?NAV_LINK_ACTIVE: NAV_LINK} onClick={()=>{setHomeLinkStatus(false)
                            setAddLinkStatus(true)
                            setAboutLinkStatus(false)
                            }}>
                            <i className="far fa-circle nav-icon"></i>
                            <p>Add a Book</p>
                            </Link>
                        </Route>
                    </li>

                    <li className="nav-item">
                        <Route>
                            <Link to="/about" className={aboutLink?NAV_LINK_ACTIVE: NAV_LINK} onClick={()=>{setHomeLinkStatus(false)
                            setAddLinkStatus(false)
                            setAboutLinkStatus(true)
                            }}>
                            <i className="far fa-circle nav-icon"></i>
                            <p>About</p>
                            </Link>
                        </Route>
                    </li>
                    
                </li>
            </ul>
            </nav>
        </ul>
    </div>
}