import {bookStore} from "../store"
import { ADD_BOOK } from "../store/constants/actionsType"
import { useHistory } from "react-router-dom";


export function ShowBook({bookData,isbn}){
    const history = useHistory();
    
    function addBook(bookData,isbn){
   
        const actionPayload={
            isbn: isbn,
            title: bookData.title,
            author: bookData.authors,
            published: bookData.publishedDate,
            publisher: bookData.publisher,
            pages: bookData.pageCount,
            description: bookData.description,
          }
        bookStore.dispatch({type:ADD_BOOK,payload:actionPayload})
        history.push("/")
    
    }

    return <div className="card card-solid">
        <div className="card-body">
          <div className="row">
            <div className="col-12 col-sm-6">
              <h3 className="d-inline-block d-sm-none">{bookData.title}</h3>
              <div className="col-12">
              <img src={"http://covers.openlibrary.org/b/isbn/"+isbn+"-L.jpg"} alt={bookData.title}/>
              </div>
              
            </div>
            <div className="col-12 col-sm-6">
              <h3 className="my-3">{bookData.title}</h3>
              <p>{bookData.description}</p>
              <div className="bg-gray py-2 px-3 mt-4">
                <h2 className="mb-0">
                  {bookData.authors.map((author)=>{return author})}
                </h2>
                <h4 className="mt-0">
                  <small>Page Count: {bookData.pageCount} </small>
                </h4>
              </div>

              <div className="mt-4">
                <div className="btn btn-primary btn-lg btn-flat" onClick={(e)=>{addBook(bookData,isbn)}}>
                  
                  ADD To my Book lib
                </div>

                
              </div>

              

            </div>
          </div>
          
        </div>
       
      </div>
}