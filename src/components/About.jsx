export function About(){
    return <div className="col-12 col-md-12 col-lg-4 order-1 order-md-2">
    <h3 className="text-primary"><i className="fas fa-paint-brush"></i>My BookLib</h3>
    <p className="text-muted">Ce projet vise a permettre une gestion facile d'une bibliothéque sur la base du numéro ISBN a ajouter manuellement ou avec un scan du Code a barre ( si la qualité de la web cam le permet)
les Données des livres sont importé grace au ISBN a partir de la lib google book ( qui n'est pas complete mais gratuite)
une version prochaine pourra donner la main a l'utilisateur du choix de l'API a utilisé .</p>
    <br/>
    <div className="text-muted">
      <p className="text-sm">Company
        <b className="d-block">GoMyCode</b>
      </p>
      <p className="text-sm">Project Leader
        <b className="d-block">Nabil DOGHRI</b>
      </p>
    </div>

    <h5 className="mt-5 text-muted">Project Resources</h5>
    <ul className="list-unstyled">
      
      <li>
        <a href="https://openlibrary.org/dev/docs/api/covers" className="btn-link text-secondary"> Open Library Covers API</a>
      </li>
      <li>
        <a href="https://developers.google.com/books/docs/overview" className="btn-link text-secondary"> Google Books API</a>
      </li>
      <li>
        <a href="https://www.npmjs.com/package/barcode-react-scanner" className="btn-link text-secondary"> barcode-react-scanner</a>
      </li>
    </ul>
    </div>
  
}

export default About