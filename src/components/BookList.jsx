import {connect} from 'react-redux'
import bookStore from '../store'
import { SUP_BOOK } from '../store/constants/actionsType'


export function BookList({bookList}){
    function delateBook(isbn){
        const delatePayload={
            isbn:isbn
        }
        bookStore.dispatch({type:SUP_BOOK,payload:delatePayload})
    }
    return <div className="col-12">
    <div className="card">
      <div className="card-header">
        <h3 className="card-title">DataTable with minimal features & hover style</h3>
      </div>
      
      <div className="card-body">
        <table id="example2" className="table table-bordered table-hover">
          <thead>
          <tr>
            <th>Title</th>
            <th>COVER</th>
            <th>Author(s)</th>
            <th>Published</th>
            <th>Publisher</th>
            <th>Pages</th>
            
            <th>ISBN</th>
            
            <td>#</td>
          </tr>
          </thead>
          <tbody>
              {bookList.map((book)=>{return <tr key={book.isbn}>
                                                <td>{book.title}</td>
                                                <td><img src={"http://covers.openlibrary.org/b/isbn/"+book.isbn+"-M.jpg"} alt={book.title}/> </td>
                                                <td>{book.author}</td>
                                                <td>{book.published}</td>
                                                <td>{book.publisher}</td>
                                                <td>{book.pages}</td>
                                                <td>{book.isbn}</td>
                                                
                                                <td><button className="btn btn-danger" onClick={(e)=>delateBook(book.isbn)}>Delete</button></td>
                                            </tr>
            })}
          
         
          </tbody>
          <tfoot>
          <tr>
            <th>Title</th>
            <th>COVER</th>
            <th>Author(s)</th>
            <th>Published</th>
            <th>Publisher</th>
            <th>Pages</th>
            
            <th>ISBN</th>
            
            <td>#</td>
          </tr>
          </tfoot>
        </table>
      </div>
      </div>
      </div>
}


export const BookListStore = connect((state)=>({
    bookList:state
}))(BookList)