
import { BookListStore } from './BookList'
import { Route,Switch } from "react-router-dom";
import { AddBook } from './AddBook';
import { About } from './About';

export function Content(){
    return <div className="content-wrapper">
    <section className="content">
      <div className="container-fluid">
        <div className="row">
          <section className="col-lg-12 connectedSortable">
            <Switch>
                <Route exact path="/" children={ <BookListStore></ BookListStore >} />
            </Switch>
            <Switch>
                <Route exact path="/addBook"  children={ <AddBook></AddBook>} />
            </Switch>
            <Switch>
                <Route exact path="/about"  children={ <About></About>} />
            </Switch>
            
          </section>
          
        </div>
      </div>
    </section>
  </div>
}