import React from 'react';
import { useState } from 'react';
import axios from 'axios';
import {ShowBook} from "./ShowBook"
import BarcodeScannerComponent from "react-webcam-barcode-scanner";




export function AddBook(){

    const  [isbn, setISBN] = useState()
    const [loading,setLoaddingStatus]  = useState(false)
    const [bookData, setBookData] = useState({})
    const [webCamVisible, setVisibilityState] = useState(false)

    async function getBookData(isbn){
        setLoaddingStatus(true)
        //const result = await axios("https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbn+"keyes&key=AIzaSyCU4Gpm0N7vrN-goNi6tD_PF_0I-N21Cbc")
        const result = await axios("https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbn)
        console.log(result.data)
        if(result.data.totalItems>0){
            setBookData(result.data)
            setLoaddingStatus(false)
        }
        
    }
    return <div className="card card-warning">
    <div className="card-header">
      <h3 className="card-title">Add a Book</h3>
    </div>
   
    <div className="card-body">
      
        <div className="row">
          <div className="col-sm-6">
        
            <div className="form-group">
              <label>ISBN</label>
              <input type="text" value ={isbn} className="form-control" placeholder="Enter valid ISBN" onChange={(e)=>setISBN(e.target.value)}/>
              <button onClick={()=>getBookData(isbn)}>Add</button>
            </div>
          </div>
          <div className="col-sm-6">
            {loading?<div>loading...</div>:bookData.totalItems>0?bookData.items.map((e)=><ShowBook bookData={e.volumeInfo} isbn={isbn}></ShowBook>):<div>Pas de données</div>}
          </div>

        </div>

        <div className="row">
        <div className="form-group">
            
              <label>Scan ISBN BARCODE</label><br/>
              <div >
             {webCamVisible && <BarcodeScannerComponent width={500} hidden={!webCamVisible}
                height={500}
                onUpdate={(err, result) => {
                if (result) setISBN(result.text)
                //else setISBN('Not Found')
             }}></BarcodeScannerComponent>} 
              </div>
              <button onClick={(e)=>setVisibilityState(!webCamVisible)}>{!webCamVisible?"Open Cam":"Close Cam"}</button>
        </div>
        </div>
        

        
        

        

       

       
      
    </div>

  </div>
}