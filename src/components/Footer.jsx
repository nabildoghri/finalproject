export function Footer(){
    return <footer className="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://www.linkedin.com/in/nabil-doghri-7a247927/">Nabil DOGHRI</a>.</strong>
    All rights reserved.
    <div className="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0-rc
    </div>
  </footer>
}