import { Menue } from './Menue';
export function SideBar(){
    return <aside className="main-sidebar sidebar-dark-primary elevation-4">
    
    <a href="index3.html" className="brand-link">
       <span className="brand-text font-weight-light">MyBookLib</span>
    </a>

   
    <div className="sidebar">
      <div className="form-inline">
        <div className="input-group" data-widget="sidebar-search">
          <input className="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
          <div className="input-group-append">
            <button className="btn btn-sidebar">
              <i className="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      <Menue></Menue>
    </div>
    </aside>
}